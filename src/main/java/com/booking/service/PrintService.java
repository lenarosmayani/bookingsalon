package com.booking.service;

import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Membership;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.repositories.ReservationRepository;


public class PrintService {
	 
    public static void printMenu(String title, String[] menuArr){
        int num = 1;
        System.out.println("==============================|" + title + "|===============================");
        
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {   
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);   
            num++;
        }
        
        System.out.println("========================================================================");
        System.out.print("Enter Your Choice : ");
    }

    public static String printServices(List<Service> serviceList){
        String result = "";
        for (Service service : serviceList) {
            result += service.getServiceName() + ", ";
        }
        return result;
    }

    public static void showRecentReservations(List<Reservation> allReservation) {
    	if (allReservation.isEmpty()) {
            System.out.println("No Reservation List available.");
            return;
        }
    	
        System.out.println("+=====================================================================================================+");
        System.out.printf("| %-4s | %-8s | %-15s | %-30s | %-15s | %-15s |\n",
                "No.", "ID", "Nama Customer", "Service", "Total Biaya", "Workstage");
        System.out.println("+=====================================================================================================+");

        int num = 1;
        for (Reservation reservation : allReservation) {
            if ("In Process".equals(reservation.getWorkstage())) {
                Customer customer = reservation.getCustomer();
                List<Service> services = reservation.getServices();
                double totalBiaya = reservation.getReservationPrice();

                System.out.printf("| %-4s | %-8s | %-15s | %-30s | %-15s | %-15s |\n",
                        num,
                        reservation.getReservationId(),
                        customer,
                        services,
                        totalBiaya,
                        reservation.getWorkstage());
                num++;
            }
        }

        System.out.println("+=====================================================================================================+");
    }
    
    public static void showAllCustomer(List<Person> personList) {
        System.out.println("+========================================================================================+");
        System.out.printf("| %-4s | %-8s | %-15s | %-15s | %-15s | %-15s |\n",
                "No.", "ID", "Nama Customer", "Alamat", "Membership", "Uang");
        System.out.println("+========================================================================================+");

        int num = 1;

        for (Person person : personList) {
            if (person instanceof Customer) {
                Customer customer = (Customer) person;
                Membership membership = customer.getMember();

                System.out.printf("| %-4s | %-8s | %-15s | %-15s | %-15s | %-15s |\n",
                        num,
                        person.getId(),
                        person.getName(),
                        person.getAddress(),
                        membership.getMembershipName(),
                        customer.getWallet());
                num++;
            }
        }
        System.out.println("+========================================================================================+");
    }


    public static void showAllEmployee(List<Person> personList) {
        System.out.println("+=======================================================================+");
        System.out.printf("| %-4s | %-8s | %-15s | %-15s | %-15s |\n",
                "No.", "ID", "Nama Employee", "Alamat", "Pengalaman");
        System.out.println("+=======================================================================+");

        int num = 1;
        for (Person person : personList) {
            if (person instanceof Employee) {
                Employee employee = (Employee) person;

                System.out.printf("| %-4s | %-8s | %-15s | %-15s | %-15s |\n",
                        num,
                        person.getId(),
                        person.getName(),
                        person.getAddress(),
                        employee.getExperience());
                num++;
            }
        }

        System.out.println("+=======================================================================+");
    }

    
    public static void showServiceList(List<Service> serviceList) {
        System.out.println("+==========================================================+");
        System.out.printf("| %-4s | %-8s | %-20s | %-15s |\n", "No.", "ID", "Nama ", "Harga");
        System.out.println("+==========================================================+");

        int num = 1; 

        for (Service service : serviceList) {
            System.out.printf("| %-4s | %-8s | %-20s | %-15s |\n",
                    num,
                    service.getServiceId(),
                    service.getServiceName(),
                    service.getPrice());
            num++;
        }

        System.out.println("+==========================================================+");

    }


    public static void showHistoryReservation(List<Reservation> reservationList){
    	 double totalProfit = 0;

         for (Reservation reservation : reservationList) {
             System.out.println(reservation);
             if ("Finish".equalsIgnoreCase(reservation.getWorkstage())) {
                 totalProfit += reservation.getReservationPrice();
             }
         }

         System.out.println("Total Profit: Rp. " + totalProfit);
     }
 }

