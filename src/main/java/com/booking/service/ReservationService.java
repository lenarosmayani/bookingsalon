package com.booking.service;

import java.util.List;
import java.util.Scanner;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Membership;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ReservationRepository;
import com.booking.repositories.ServiceRepository;

public class ReservationService {

	private static List<Person> personList = PersonRepository.getAllPerson();
    private static List<Service> serviceList = ServiceRepository.getAllService();
    private static ReservationRepository reservationRepository = new ReservationRepository();
    private static Scanner input = new Scanner(System.in);

    public void createReservation() {
        Customer selectedCustomer = selectCustomer();
        Employee selectedEmployee = selectEmployee();
        List<Service> selectedServices = selectServices();
        
        Reservation reservation = Reservation.builder()
                .customer(selectedCustomer)
                .employee(selectedEmployee)
                .services(selectedServices)
                .workstage("In Process")
                .reservationPrice(calculateReservationPrice(selectedCustomer, selectedServices))
                .build();

		reservationRepository.addReservation(reservation);

        System.out.println("Booking Successful!");
        System.out.println("Total Booking Cost: Rp. " + reservation.getReservationPrice());
        
    }

    private static Customer selectCustomer() {
        System.out.println("=== Select Customer ===");
        PrintService.showAllCustomer(personList);

        String customerID;
        Customer selectedCustomer;

        do {
            System.out.print("Enter Customer ID: ");
            customerID = input.nextLine();
            selectedCustomer = findCustomerByID(customerID, personList);

            if (!ValidationService.validateInputID(customerID)) {
                System.out.println("Invalid Customer ID. Please enter a valid ID.");
            } else {
                System.out.println("Customer : " + customerID + " " + selectedCustomer);
            }

        } while (!ValidationService.validateInputID(customerID));

        return selectedCustomer;
    }

    private static Employee selectEmployee() {
        System.out.println("=== Select Employee ===");
        PrintService.showAllEmployee(personList);

        String employeeID;
        Employee selectedEmployee;

        do {
            System.out.println("Enter Employee ID : ");
            employeeID = input.nextLine();
            selectedEmployee = findEmployeeByID(employeeID, personList);

            if (!ValidationService.validateInputID(employeeID)) {
                System.out.println("Invalid Employee ID. Please enter a valid ID.");
            } else {
                System.out.println("Employee : " + employeeID + " " + selectedEmployee);
            }

        } while (!ValidationService.validateInputID(employeeID));

        return selectedEmployee;
    }

    private static List<Service> selectServices() {
        System.out.println("=== Select Services ===");
        PrintService.showServiceList(serviceList);

        String continueSelection;

        do {
            System.out.print("Enter Service ID: ");
            String serviceID = input.nextLine();
            Service selectedService = findServiceByID(serviceID);

            if (selectedService != null) {
                if (!serviceList.contains(selectedService)) {
                	serviceList.add(selectedService);
                    System.out.println("Service '" + selectedService.getServiceName() + "' has been added.");
                } else {
                    System.out.println("Service '" + selectedService.getServiceName() + "' has already been selected.");
                }
            } else {
                System.out.println("Error: Service with ID '" + serviceID + "' not found.");
            }

            System.out.print("Continue selecting services? (Y/N): ");
            continueSelection = input.nextLine();

        } while ("Y".equalsIgnoreCase(continueSelection));

        return serviceList;
    }

    private static Customer findCustomerByID(String customerID, List<Person> personList) {
    	for (Person person : personList) {
            if (person instanceof Customer && person.getId().equalsIgnoreCase(customerID)) {
                return (Customer) person;
            }
        }
        return null;
    }

    private static Employee findEmployeeByID(String employeeID, List<Person> personList) {
    	for (Person person : personList) {
            if (person instanceof Employee && person.getId().equalsIgnoreCase(employeeID)) {
                return (Employee) person;
            }
        }
        return null;
    }

    private static Service findServiceByID(String serviceID) {
        for (Service service : serviceList) {
            if (service.getServiceId().equalsIgnoreCase(serviceID)) {
                return service;
            }
        }
        return null;
    }

    private double calculateReservationPrice(Customer customer,  List<Service> serviceList) {
        double totalPrice = 0;

        for (Service service : serviceList) {
            double price = service.getPrice();
            Membership membership = customer.getMember();

            switch (membership.getMembershipName().toUpperCase()) {
                case "NONE":
                    totalPrice = price;
                    break;
                case "SILVER":
                    totalPrice = price * (1 - 0.05);
                    break;
                case "GOLD":
                    totalPrice = price * (1 - 0.1);
                    break;
            }
        }

        return totalPrice;
    }

    
 public void cancelReservation(List<Reservation> reservationList) {
        System.out.println("Reservations with 'In Process' Workstage:");
        for (Reservation reservation : reservationList) {
            if ("In Process".equalsIgnoreCase(reservation.getWorkstage())) {
                System.out.println(reservation);
            }
        }

        // Input reservation ID yang akan dibatalkan
        System.out.print("Enter Reservation ID to cancel (or '0' to go back): ");
        String cancelReservationID = input.nextLine();

        // Jika pengguna memilih untuk kembali
        if ("0".equals(cancelReservationID)) {
            System.out.println("Returning to the previous menu.");
            return;
        }

        // Cari reservation sesuai ID
        Reservation selectedReservation = findReservationByID(reservationList, cancelReservationID);

        // Jika reservation tidak ditemukan
        if (selectedReservation == null) {
            System.out.println("Error: Reservation with ID '" + cancelReservationID + "' not found.");
            return;
        }

        // Jika reservation ditemukan dan memiliki workstage "In Process"
        if ("In Process".equalsIgnoreCase(selectedReservation.getWorkstage())) {
            System.out.println("Cancelling Reservation...");
            // Tambahkan logika pembatalan reservasi di sini

            System.out.println("Reservation with ID '" + cancelReservationID + "' has been cancelled.");
        } else {
            System.out.println("Error: Reservation with ID '" + cancelReservationID + "' is not in 'In Process' workstage.");
        }
    }

    private Reservation findReservationByID(List<Reservation> reservationList, String reservationID) {
        for (Reservation reservation : reservationList) {
            if (reservation.getReservationId().equalsIgnoreCase(reservationID)) {
                return reservation;
            }
        }
        return null;
    }
}