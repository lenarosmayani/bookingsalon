package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.repositories.PersonRepository;

public class MenuService {
    private static List<Person> personList = PersonRepository.getAllPerson();
    private static List<Reservation> allReservation = new ArrayList<>();
    private static Scanner input = new Scanner(System.in);

    public static void mainMenu(String selectCustomerID, String selectEmployeeID) {
        String[] mainMenuArr = {"Show Data", "Create Reservation", "Complete/Cancel Reservation", "Exit"};
        String[] subMenuArr = {"Recent Reservation", "Show Customer", "Show Available Employee", "Show History Reservation", "Back to Main Menu"};

        int optionMainMenu;
        int optionSubMenu;

        ReservationService reservationService = new ReservationService(); 

        do {
            PrintService.printMenu("Main Menu", mainMenuArr);
            optionMainMenu = Integer.valueOf(input.nextLine());
            System.out.println();

            switch (optionMainMenu) {
                case 1:
                    do {
                        PrintService.printMenu("Show Data", subMenuArr);
                        optionSubMenu = Integer.valueOf(input.nextLine());
                        System.out.println();

                        // Sub menu - menu 1
                        switch (optionSubMenu) {
                            case 1:
                                PrintService.showRecentReservations(allReservation);
                                break;
                            case 2:
                                PrintService.showAllCustomer(personList);
                                break;
                            case 3:
                                PrintService.showAllEmployee(personList);
                                break;
                            case 4:
                                PrintService.showHistoryReservation(allReservation);
                                break;
                            case 0:
                                break;
                            default:
                                System.out.println("Invalid option. Please enter a valid option.");
                                break;
                        }
                    } while (optionSubMenu != 0);
                    break;

                case 2:
                    reservationService.createReservation();
                    break;
                case 3:
                    reservationService.cancelReservation(allReservation);
                    break;
                case 0:
                    System.out.println("Exiting the program...");
                    break;
                default:
                    System.out.println("Invalid option. Please enter a valid option.");
                    break;
            }
        } while (optionMainMenu != 0);
    }
}
