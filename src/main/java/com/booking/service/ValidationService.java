package com.booking.service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationService {
	public static boolean validateInputID(String inputData) {
	        if (!isNullOrEmpty(inputData)) {
	            Pattern pattern = Pattern.compile("[a-zA-Z0-9\\s-]+");
	            Matcher matcher = pattern.matcher(inputData);
	            return matcher.matches();
	        }
	        return false;
	    }

    public static boolean validateInputString(String inputData) {
        if (!isNullOrEmpty(inputData)) {
            Pattern pattern = Pattern.compile("[a-zA-Z\\s]+");
            Matcher matcher = pattern.matcher(inputData);
            return matcher.matches();
        }
        return false;
    }
    
    public static boolean validateInputNumeric(String inputData) {
        if (!isNullOrEmpty(inputData)) {
            Pattern pattern = Pattern.compile("\\d+");
            Matcher matcher = pattern.matcher(inputData);
            return matcher.matches();
        }
        return false;
    }
    
    private static boolean isNullOrEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }
    
}