package com.booking.repositories;

import java.util.ArrayList;
import java.util.List;

import com.booking.models.Reservation;

public class ReservationRepository {
	 	private List<Reservation> allReservation;
	    private static int reservationCounter = 0;

	    public ReservationRepository() {
	        this.allReservation = new ArrayList<>();
	    }

	    public void addReservation(Reservation reservation) {
	    	reservation.setReservationId(generateUniqueReservationID());
	        this.allReservation.add(reservation);
	    }
	    
	    public void cancelReservation(Reservation reservasi) {
	        this.allReservation.remove(reservasi);
	    }
	    
	    public Reservation findReservation(String reservationID) {
	        for (Reservation reservation : allReservation) {
	            if (reservation.getReservationId().equals(reservationID)) {
	                return reservation;
	            }
	        }
	        return null;
	    }

	    public List<Reservation> getAllReservation() {
	        return new ArrayList<>(this.allReservation);
	    }

	    private String generateUniqueReservationID() {
	        reservationCounter++;
	        return "Rsv-" + String.format("%02d", reservationCounter);
	    }

}
